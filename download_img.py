import urllib
import requests
from bs4 import BeautifulSoup
from os.path import dirname
import re
from PIL import Image
from io import BytesIO

def get_image_size(url):
    data = requests.get(url).content
    im = Image.open(BytesIO(data))    
    return im.size

def get_soup(url):
    source_code = requests.get(url)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, "html.parser")
    return soup

def getLinks(url):
    soup = get_soup(url)
    links = soup.find_all('a')

    links=list(map(lambda tag:tag.get('href',None),links))
    links=list(filter(lambda link:True if (link is not None) else False,links))
    links=list(set(links))
    links.sort()
    links=list(filter(lambda link:True if (link[0]!='#') else False,links))
    links=list(filter(lambda link:True if (link[0:3]!='../') else False,links))
    links=list(filter(lambda link:True if (link[-4:]=='.htm') else False,links))
    links=list(filter(lambda link:True if (re.search('\d+', link)) else False,links))
    links=list(map(lambda link:dirname(url)+'/'+link,links))
    return links
    
def filter_imgs_by_size(links):
    links_copy=links.copy()
    for link in links_copy:
        width, height=get_image_size(link)
        if (width<400) or (height<150):
            links_copy.remove(link)
    return links_copy


def get_list_of_imgs_from_url(url,soup):
    links=[]
    for link in soup.findAll('img'):
        image_links = link.get('src')
        for i in image_links.split("\\n"):
            links.append(dirname(url)+'/'+i)
    links=filter_imgs_by_size(links)
    return links

def download_imgs_from_file(links):
    global name
    for link in links:
        name+=1
        fullName = 'photos/'+str(name) + ".jpg"
        urllib.request.urlretrieve(link, fullName)
        print(link+'\n'+fullName+'\n')

def Download_Image_from_Web(url):
    soup=get_soup(url)

    links=get_list_of_imgs_from_url(url,soup)[:1]
    pass
    download_imgs_from_file(links)

name=529

#Download_Image_from_Web("http://www.banknote.ws/COLLECTION/countries/AME/HON/HON0045.htm")


# all_pages=['http://www.banknote.ws/COLLECTION/countries/EUR/UKR/UKR-NBU.htm',
#            'http://www.banknote.ws/COLLECTION/countries/EUR/SOV/SOV-STA.htm',
#            'http://www.banknote.ws/COLLECTION/countries/EUR/FRA/FRA-BDF.htm',
#            'http://www.banknote.ws/COLLECTION/countries/EUR/GFR/GFR.htm',
#            'http://www.banknote.ws/COLLECTION/countries/AME/USA/USA-FEDRES/USA-FEDRES.htm',
#            'http://www.banknote.ws/COLLECTION/countries/AME/BRA/BRA-BCB.htm']

all_pages=['http://www.banknote.ws/COLLECTION/countries/AUS/AUS/AUS-COM.htm',
           'http://www.banknote.ws/COLLECTION/countries/AUS/SOL/SOL.htm',
           'http://www.banknote.ws/COLLECTION/countries/ASI/JAP/JAP-NPG.htm',
           'http://www.banknote.ws/COLLECTION/countries/ASI/ISR/ISR.htm',
           'http://www.banknote.ws/COLLECTION/countries/ASI/TUR/TUR-REP.htm',
           'http://www.banknote.ws/COLLECTION/countries/ASI/PAK/PAK-SBP.htm',
           'http://www.banknote.ws/COLLECTION/countries/ASI/SAR/SAR.htm'
           ]

for page in all_pages:
    links=getLinks(page)
    for link in links:
        Download_Image_from_Web(link)





