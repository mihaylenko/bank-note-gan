import tensorflow as tf
import matplotlib.pyplot as plt
from PIL import Image
import io

size_of_image=[40,80]

class Display:

    def __init__(self):
        my_dpi=100
        plt.ion()
        plt.grid(False)
        plt.axis('off')
        #plt.figure(figsize=(size_of_image[0]/my_dpi, size_of_image[1]/my_dpi), dpi=my_dpi)

    def show(self,byte_string):
        plt.clf()
        plt.imshow(Image.open(io.BytesIO(byte_string)))
        plt.draw()
        plt.pause(0.001)


learning_rate = tf.constant(0.0005,dtype=tf.float32)
padding="same"

def _parse_function(filename):
    image_string = tf.read_file(filename)
    image_decoded = tf.image.decode_jpeg(image_string,channels=3)
    image_resized = tf.image.resize_images(image_decoded, size_of_image)#320//2, 640//2
    image_normalized=(image_resized-127.5)/127.5

    return image_normalized


filenames = tf.train.match_filenames_once('photos/*.jpg')
dataset = tf.data.Dataset.from_tensor_slices(filenames)
dataset = dataset.apply(tf.data.experimental.shuffle_and_repeat(buffer_size = 500, count = 10000))
dataset = dataset.map(_parse_function)

dataset=dataset.batch(batch_size = 5)
dataset = dataset.prefetch(buffer_size=1)


iterator = dataset.make_initializable_iterator()
images=iterator.get_next()



#Generator
def Generator(reuse=False):
    with tf.variable_scope('generator', reuse=reuse):
        input=tf.random.normal(shape=[tf.placeholder(dtype=tf.int32,shape=[],name='batch_size'),100])
        d_1=tf.layers.dense(input,5*10*128)
        re_1 = tf.reshape(d_1, (-1, 5, 10, 128))
        ct_1=tf.layers.conv2d_transpose(inputs=re_1,filters=64,kernel_size=3, strides=2,
                                        padding='same',activation=tf.nn.relu,
                                        kernel_initializer=tf.contrib.layers.xavier_initializer())
        ct_2=tf.layers.conv2d_transpose(inputs=ct_1,filters=32,kernel_size=3, strides=2,
                                        padding='same',activation=tf.nn.relu,
                                        kernel_initializer=tf.contrib.layers.xavier_initializer())
        ct_3=tf.layers.conv2d_transpose(inputs=ct_2,filters=3,kernel_size=5, strides=2,
                                        padding='same',activation=tf.nn.tanh,#Вконце активация tanh
                                        kernel_initializer=tf.contrib.layers.xavier_initializer())
        # ct_4=tf.layers.conv2d_transpose(inputs=ct_3,filters=3,kernel_size=5, strides=2,
        #                                 padding='same',activation=tf.nn.tanh,
        #                                 kernel_initializer=tf.contrib.layers.xavier_initializer())
        return ct_3



#Discriminator

def Discriminator(input,reuse=False):
    with tf.variable_scope('discriminator', reuse=reuse):
        c_1=tf.layers.conv2d(inputs=input,filters=32,kernel_size=5,padding='same',
                             activation=tf.nn.leaky_relu,strides=2,
                             kernel_initializer=tf.contrib.layers.xavier_initializer())
        c_2=tf.layers.conv2d(inputs=c_1,filters=64,kernel_size=5,padding='same',
                             activation=tf.nn.leaky_relu,strides=2,
                             kernel_initializer=tf.contrib.layers.xavier_initializer())
        c_3=tf.layers.conv2d(inputs=c_2,filters=128,kernel_size=5,padding='same',
                             activation=tf.nn.leaky_relu,strides=2,
                             kernel_initializer=tf.contrib.layers.xavier_initializer())
        re_1=tf.reshape(c_3, (-1, 5*10*128))
        d_1=tf.layers.dense(re_1,1)
        sigmoid=tf.nn.sigmoid(d_1)
        return sigmoid,d_1



G_sample=Generator()
D_real, D_logit_real=Discriminator(images)
D_fake, D_logit_fake=Discriminator(G_sample,reuse=True)



d_loss_real = tf.losses.sigmoid_cross_entropy(logits=D_logit_real, 
                    multi_class_labels=tf.ones_like(D_real)+tf.random.uniform(shape=tf.shape(D_real),minval=-0.1,maxval=0.0))
d_loss_fake = tf.losses.sigmoid_cross_entropy(logits=D_logit_fake, 
                    multi_class_labels=tf.zeros_like(D_fake)+tf.random.uniform(shape=tf.shape(D_fake),minval=0.0,maxval=0.1))
d_loss=d_loss_real+d_loss_fake

g_loss = tf.losses.sigmoid_cross_entropy(logits=D_logit_fake, multi_class_labels=tf.ones_like(D_fake))


g_optimizer=tf.train.AdamOptimizer(learning_rate).minimize(g_loss, 
            var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='generator'))
d_optimizer=tf.train.AdamOptimizer(learning_rate).minimize(d_loss, 
            var_list=tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='discriminator'))


def get_image_to_show(G_sample):
    image_reshaped=tf.reshape(G_sample, (*size_of_image, 3))
    image_scaled=image_reshaped*127.5+127.5
    image_encoded=tf.image.encode_jpeg(tf.cast(image_scaled,dtype=tf.uint8))
    return image_encoded

image_encoded=get_image_to_show(G_sample)

display=Display()



with tf.Session() as sess:

    saver = tf.train.Saver(tf.global_variables(),#Saves also various optimizers
                           max_to_keep = 1000,
                           #filename='settings\\variables',
                           name='saver')

    saver.restore(sess=sess,save_path='recovery data\\gan')
    #sess.run(tf.global_variables_initializer())
    
    sess.run(tf.local_variables_initializer()) 
    sess.run(iterator.initializer)


    

    # for i in range(100):
    #     display.show(sess.run(image_strings)[0])



    #print(sess.run(filenames))
    
    while True:
        try:
            for _ in range(200):
                loss,_=sess.run([d_loss,d_optimizer],feed_dict={'generator/batch_size:0':5})
                print('D loss',loss)
            for _ in range(100):
                loss,_=sess.run([g_loss,g_optimizer],feed_dict={'generator/batch_size:0':5})
                print('G loss',loss)
            



            #print(sess.run(D_fake,feed_dict={'generator/batch_size:0':100}))

            #print('GAN',sess.run(G_sample,feed_dict={'generator/batch_size:0':1}))
            #print('real image',sess.run(images,feed_dict={'generator/batch_size:0':1})[0])
            img_string=sess.run(image_encoded,feed_dict={'generator/batch_size:0':1})
            display.show(img_string)
            saver.save(sess, 'recovery data\\gan',
                   latest_filename='variables_ckpt')
        except tf.errors.OutOfRangeError:
            break